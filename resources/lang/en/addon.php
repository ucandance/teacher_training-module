<?php

return [
    'title'       => 'Teacher Training',
    'name'        => 'Teacher Training Module',
    'description' => 'Manage teacher training resources for UCanDance.'
];

<?php

return [
	"name" => [
		"name" => "Name",
	],
	"slug" => [
		"name" => "Slug",
		"instructions" => "This field is automatically generated. Do not change.",
	],
	"category" => [
		"name" => "Module Category",
	],
	"video_id" => [
		"name" => "YouTube Video ID",
		"instructions" => "The Kf7S9BRJ8nk part from a YouTube URL like this: https://www.youtube.com/watch?v=Kf7S9BRJ8nk"
	],
	"content" => [
		"name" => "Module Content",
	],
	"image" => [
		"name" => "Module Image",
	],
	"description" => [
		"name" => "Description",
	],
];

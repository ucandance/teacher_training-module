<?php

return [
    'module_categories' => [
        'name'   => 'Module categories',
        'option' => [
            'read'   => 'Can read module categories?',
            'write'  => 'Can create/edit module categories?',
            'delete' => 'Can delete module categories?',
        ],
    ],
    'modules' => [
        'name'   => 'Modules',
        'option' => [
            'read'   => 'Can read modules?',
            'write'  => 'Can create/edit modules?',
            'delete' => 'Can delete modules?',
        ],
    ],
    'modules' => [
        'name'   => 'Modules',
        'option' => [
            'read'   => 'Can read modules?',
            'write'  => 'Can create/edit modules?',
            'delete' => 'Can delete modules?',
        ],
    ],
    'categories' => [
        'name'   => 'Categories',
        'option' => [
            'read'   => 'Can read categories?',
            'write'  => 'Can create/edit categories?',
            'delete' => 'Can delete categories?',
        ],
    ],
];

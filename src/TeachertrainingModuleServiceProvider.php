<?php namespace Finnito\TeacherTrainingModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\TeacherTrainingModule\Category\Contract\CategoryRepositoryInterface;
use Finnito\TeacherTrainingModule\Category\CategoryRepository;
use Anomaly\Streams\Platform\Model\TeacherTraining\TeacherTrainingCategoriesEntryModel;
use Finnito\TeacherTrainingModule\Category\CategoryModel;
use Finnito\TeacherTrainingModule\Module\Contract\ModuleRepositoryInterface;
use Finnito\TeacherTrainingModule\Module\ModuleRepository;
use Anomaly\Streams\Platform\Model\TeacherTraining\TeacherTrainingModulesEntryModel;
use Finnito\TeacherTrainingModule\Module\ModuleModel;
use Finnito\TeacherTrainingModule\ModuleCategory\Contract\ModuleCategoryRepositoryInterface;
use Finnito\TeacherTrainingModule\ModuleCategory\ModuleCategoryRepository;
use Anomaly\Streams\Platform\Model\TeacherTraining\TeacherTrainingModuleCategoriesEntryModel;
use Finnito\TeacherTrainingModule\ModuleCategory\ModuleCategoryModel;
use Illuminate\Routing\Router;

class TeacherTrainingModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/teacher_training'           => 'Finnito\TeacherTrainingModule\Http\Controller\Admin\ModulesController@index',
        'admin/teacher_training/create'    => 'Finnito\TeacherTrainingModule\Http\Controller\Admin\ModulesController@create',
        'admin/teacher_training/edit/{id}' => 'Finnito\TeacherTrainingModule\Http\Controller\Admin\ModulesController@edit',
        'admin/teacher_training/categories'           => 'Finnito\TeacherTrainingModule\Http\Controller\Admin\CategoriesController@index',
        'admin/teacher_training/categories/create'    => 'Finnito\TeacherTrainingModule\Http\Controller\Admin\CategoriesController@create',
        'admin/teacher_training/categories/edit/{id}' => 'Finnito\TeacherTrainingModule\Http\Controller\Admin\CategoriesController@edit',

        'teacher-training' =>                       'Finnito\TeacherTrainingModule\Http\Controller\TeacherTrainingController@index',
        'teacher-training/{category}' =>            'Finnito\TeacherTrainingModule\Http\Controller\TeacherTrainingController@category',
        'teacher-training/{category}/{module}' =>   'Finnito\TeacherTrainingModule\Http\Controller\TeacherTrainingController@module',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\TeacherTrainingModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\TeacherTrainingModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\TeacherTrainingModule\Event\ExampleEvent::class => [
        //    Finnito\TeacherTrainingModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\TeacherTrainingModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        TeacherTrainingCategoriesEntryModel::class => CategoryModel::class,
        TeacherTrainingModulesEntryModel::class => ModuleModel::class,
        TeacherTrainingModulesEntryModel::class => ModuleModel::class,
        TeacherTrainingModuleCategoriesEntryModel::class => ModuleCategoryModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        CategoryRepositoryInterface::class => CategoryRepository::class,
        ModuleRepositoryInterface::class => ModuleRepository::class,
        ModuleRepositoryInterface::class => ModuleRepository::class,
        ModuleCategoryRepositoryInterface::class => ModuleCategoryRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}

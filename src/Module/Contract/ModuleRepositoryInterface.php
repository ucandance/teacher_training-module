<?php namespace Finnito\TeacherTrainingModule\Module\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ModuleRepositoryInterface extends EntryRepositoryInterface
{
    public function findByCategoryIDAndModuleSlug($categoryID, $moduleSlug);
}

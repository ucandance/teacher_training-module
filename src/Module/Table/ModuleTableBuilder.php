<?php namespace Finnito\TeacherTrainingModule\Module\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class ModuleTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        "name",
        "category" => [
            "wrapper" => "{value.name}",
            "value" => [
                "name" => "entry.category.name",
            ],
            "heading" => "Category",
        ],
        'updated' => [
            'wrapper'  => '
                <small class="text-muted">{value.author} | {value.ts}</small>
                <br>
                <span>{value.commit}</span>',
            'value' => [
                'author' => 'entry.updated_by.display_name',
                'ts'     => 'entry.updated_at',
                'commit'     => 'entry.commit',
            ],
            "heading" => "Last Update",
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}

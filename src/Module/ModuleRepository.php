<?php namespace Finnito\TeacherTrainingModule\Module;

use Finnito\TeacherTrainingModule\Module\Contract\ModuleRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ModuleRepository extends EntryRepository implements ModuleRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ModuleModel
     */
    protected $model;

    /**
     * Create a new ModuleRepository instance.
     *
     * @param ModuleModel $model
     */
    public function __construct(ModuleModel $model)
    {
        $this->model = $model;
    }

    public function findByCategoryIDAndModuleSlug($categoryID, $moduleSlug)
    {
        return $this->model
            ->where('category_id', $categoryID)
            ->where("slug", $moduleSlug)
            ->first();
    }
}

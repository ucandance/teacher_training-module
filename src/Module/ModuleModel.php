<?php namespace Finnito\TeacherTrainingModule\Module;

use Finnito\TeacherTrainingModule\Module\Contract\ModuleInterface;
use Anomaly\Streams\Platform\Model\TeacherTraining\TeacherTrainingModulesEntryModel;

class ModuleModel extends TeacherTrainingModulesEntryModel implements ModuleInterface
{

}

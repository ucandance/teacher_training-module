<?php namespace Finnito\TeacherTrainingModule\Http\Controller\Admin;

use Finnito\TeacherTrainingModule\ModuleCategory\Form\ModuleCategoryFormBuilder;
use Finnito\TeacherTrainingModule\ModuleCategory\Table\ModuleCategoryTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ModuleCategoriesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ModuleCategoryTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ModuleCategoryTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ModuleCategoryFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ModuleCategoryFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ModuleCategoryFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ModuleCategoryFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}

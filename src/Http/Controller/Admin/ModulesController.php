<?php namespace Finnito\TeacherTrainingModule\Http\Controller\Admin;

use Finnito\TeacherTrainingModule\Module\Form\ModuleFormBuilder;
use Finnito\TeacherTrainingModule\Module\Table\ModuleTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ModulesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ModuleTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ModuleTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ModuleFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ModuleFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ModuleFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ModuleFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}

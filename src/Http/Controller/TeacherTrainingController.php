<?php namespace Finnito\TeacherTrainingModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Finnito\TeacherTrainingModule\Category\Contract\CategoryRepositoryInterface;
use Finnito\TeacherTrainingModule\Module\Contract\ModuleRepositoryInterface;
use Illuminate\Contracts\Auth\Guard;

class TeacherTrainingController extends PublicController
{

    public function index(CategoryRepositoryInterface $categories, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        $this->template->set("meta_title", "Teacher Training");
        $this->breadcrumbs->add("Teacher Training", $this->request->path());
        $allCategories = $categories->all();
        return $this->view->make(
            'finnito.module.teacher_training::public/index',
            [
                "categories" => $allCategories,
            ]
        );
    }

    public function category(
        CategoryRepositoryInterface $categories,
        Guard $auth,
        $categorySlug
    ) {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        if (!$category = $categories->findBy("slug", $categorySlug)) {
            abort(404);
        }
        $this->breadcrumbs->add("Teacher Training", "/teacher-training");
        $this->breadcrumbs->add($category->name, $this->request->path());
        $allCategories = $categories->all();
        $this->template->set("meta_title", $category->name);
        $this->template->set('edit_link', "/admin/teacher_training/categories/edit/".$category->id);
        $this->template->set("edit_type", $category->name);
        return $this->view->make(
            'finnito.module.teacher_training::public/category',
            [
                "category" => $category,
                "categories" => $allCategories,
            ]
        );
    }

    public function module(
        ModuleRepositoryInterface $modules,
        CategoryRepositoryInterface $categories,
        Guard $auth,
        $categorySlug,
        $moduleSlug
    ) {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        if (!$category = $categories->findBy("slug", $categorySlug)) {
            abort(404);
        }

        if (!$module = $modules
            ->findByCategoryIDAndModuleSlug(
                $category->id,
                $moduleSlug
            )
        ) {
            abort(404);
        }

        $this->template->set("meta_title", $module->name);
        $this->breadcrumbs->add("Teacher Training", "/teacher-training");
        $this->breadcrumbs->add($category->name, "/teacher-training/".$category->slug);
        $this->breadcrumbs->add($module->name, $this->request->path());
        $this->template->set('edit_link', "/admin/teacher_training/edit/".$module->id);
        $this->template->set("edit_type", $module->name);
        return $this->view->make(
            'finnito.module.teacher_training::public/module',
            [
                "module" => $module,
                "category" => $category,
            ]
        );
    }
}

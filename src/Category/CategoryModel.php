<?php namespace Finnito\TeacherTrainingModule\Category;

use Finnito\TeacherTrainingModule\Category\Contract\CategoryInterface;
use Anomaly\Streams\Platform\Model\TeacherTraining\TeacherTrainingCategoriesEntryModel;
use Finnito\TeacherTrainingModule\Module\ModuleModel;

class CategoryModel extends TeacherTrainingCategoriesEntryModel implements CategoryInterface
{
    public function modules()
    {
        return $this->hasMany(ModuleModel::class, "category_id")->get();
    }
}

<?php namespace Finnito\TeacherTrainingModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class TeacherTrainingModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-book';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'modules' => [
            'buttons' => [
                'new_module',
            ],
        ],
        'categories' => [
            'buttons' => [
                'new_category',
            ],
        ],
    ];

}
